#!/usr/bin/python3
# -*- coding: utf-8 -*-

import contextlib
from io import StringIO
import os
import sys
import unittest

from unittest.mock import patch

import sortwords

this_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.join(this_dir, '..')

class TestIsLower(unittest.TestCase):

    def test_1upper(self):
        self.assertEqual(sortwords.is_lower("A", "B"), True)
        self.assertEqual(sortwords.is_lower("A", "A"), False)
        self.assertEqual(sortwords.is_lower("B", "A"), False)

    def test_1lower(self):
        self.assertEqual(sortwords.is_lower("a", "b"), True)
        self.assertEqual(sortwords.is_lower("a", "a"), False)
        self.assertEqual(sortwords.is_lower("b", "a"), False)

    def test_1mixed(self):
        self.assertEqual(sortwords.is_lower("a", "B"), True)
        self.assertEqual(sortwords.is_lower("A", "a"), False)
        self.assertEqual(sortwords.is_lower("B", "a"), False)

    def test_2mixed(self):
        self.assertEqual(sortwords.is_lower("aa", "BB"), True)
        self.assertEqual(sortwords.is_lower("Aa", "a"), False)
        self.assertEqual(sortwords.is_lower("B", "aB"), False)
        self.assertEqual(sortwords.is_lower("Ba", "bb"), True)

    def test_3mixed(self):
        self.assertEqual(sortwords.is_lower("aaa", "BBB"), True)
        self.assertEqual(sortwords.is_lower("Aaa", "a"), False)
        self.assertEqual(sortwords.is_lower("B", "aBC"), False)
        self.assertEqual(sortwords.is_lower("Bba", "bbb"), True)

    def test_difficult(self):
        self.assertEqual(sortwords.is_lower("bac", "adc"), False)
        self.assertEqual(sortwords.is_lower("bac", "ad"), False)
        self.assertEqual(sortwords.is_lower("ba", "adc"), False)
        self.assertEqual(sortwords.is_lower("b", "adc"), False)
        self.assertEqual(sortwords.is_lower("ba", "a"), False)
        self.assertEqual(sortwords.is_lower("a", "ba"), True)


class TestGetLower(unittest.TestCase):

    def test_0(self):
        self.assertEqual(sortwords.get_lower(["a", "b", "c"], 0), 0)
        self.assertEqual(sortwords.get_lower(["b", "a", "c"], 0), 1)
        self.assertEqual(sortwords.get_lower(["b", "c", "a"], 0), 2)

    def test_1(self):
        self.assertEqual(sortwords.get_lower(["a", "b", "c"], 1), 1)
        self.assertEqual(sortwords.get_lower(["b", "a", "c"], 1), 1)
        self.assertEqual(sortwords.get_lower(["b", "c", "a"], 1), 2)

    def test_last(self):
        self.assertEqual(sortwords.get_lower(["a", "b", "c"], 2), 2)
        self.assertEqual(sortwords.get_lower(["b", "a", "c"], 2), 2)
        self.assertEqual(sortwords.get_lower(["b", "c", "a"], 2), 2)

class TestSort():

    def test_sort_2(self):
        self.assertEqual(sortwords.sort(["A", "B"]), ["A", "B"])
        self.assertEqual(sortwords.sort(["B", "A"]), ["A", "B"])
        self.assertEqual(sortwords.sort(["BA", "AA"]), ["AA", "BA"])
        self.assertEqual(sortwords.sort(["B", "AA"]), ["AA", "B"])

    def test_sort_3(self):
        self.assertEqual(sortwords.sort(["A", "B", "c"]), ["A", "B", "c"])
        self.assertEqual(sortwords.sort(["B", "A", "c"]), ["A", "B", "c"])
        self.assertEqual(sortwords.sort(["BA", "AA", "CC"]), ["AA", "BA", "CC"])
        self.assertEqual(sortwords.sort(["B", "CC", "AA"]), ["AA", "B", "CC"])


if __name__ == '__main__':
    unittest.main(module=__name__, buffer=True, exit=False)
